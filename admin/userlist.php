<?php
include_once("common_header.php");
if (!isset($_errorMessage)){
  $_errorMessage = "";
}
if (!isset($_completeMessage)){
  $_completeMessage = "";
}
?>
<!-- DataTables -->
<link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap4.css">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h4>会員管理</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="top.php">Top</a></li>
              <li class="breadcrumb-item">会員管理</li>
              <li class="breadcrumb-item active">会員一覧</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
<?php
if ($_completeMessage != ""){
?>
    <div class="alert alert-info alert-dismissible fade show" role="alert">
      <?= $_completeMessage ?>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
 <?php
  $_completeMessage = "";
}
?>
        <div class="card card-primary card-outline">
            <div class="card-header">
              <h3 class="card-title">会員一覧</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="membertable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID.</th>
                  <th>氏名</th>
                  <th><small>E-Mail</small></th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
<?php
  $userlist = $userDbh->doSelectAll();
  if (count($userlist) > 0){
    foreach($userlist as $idx => $row){
      $id = $row['id'];
      $deltarget = sprintf("%sさん",$row['username']);
      $delurl = sprintf("%s?mode=DELETE&id=%d", $_SERVER['PHP_SELF'], $row['id']);
  ?>

                  <tr>
                    <td><div align="right"><?= $row['id'] ?></div></td>
                    <td><?= $row['username'] ?></td>
                    <td><small><?= $row['email'] ?></small></td>
                    <td>
                      <div align="center">
                      <a class="btn btn-outline-danger" href="javascript:void(0)" id="confirm" onclick="confirmDelete('<?= $deltarget ?>','<?= $delurl ?>'); return false;"><i class="far fa-trash-alt"></i></a>
                      </div>
                    </td>
                  </tr>
  <?php
    }
  }
  ?>
              </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php
include_once("common_footer.php");
?>
<!-- DataTables -->
<script src="../plugins/datatables/jquery.dataTables.js"></script>
<script src="../plugins/datatables/dataTables.bootstrap4.js"></script>
<script src="../common/js/bootbox.js"></script>
<script>
  $(function () {
    $('#membertable').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false,
      "columnDefs": [
        { "orderable": false, "targets": [2]},
        { "orderable": false, "targets": [3]},
         ],
    })
  })
  function confirmDelete(target, url){
    //alert("confirmDelete");
    bootbox.dialog({
  　　message: target+"を削除してもよろしいですか？",
      title: "",
      buttons: {
          main: {
          label: "閉じる",
              className: "btn-default",
                  callback: function() {
                      console.log("閉じる");
                  }
              },
              danger: {
                  label: "OK",
                  className: "btn-danger",
                  callback: function() {
                    location.href=url;
                        console.log("削除する");
              }
        }
      }
    });
  }
</script>
</body>
</html>
