<?php
require_once("../inc/phpini.php");
include_once("DBManager.class.php");

$g_AdminTop = $gSys->getProperty("system", "adminurl");
$g_Title = $gSys->getProperty("system", "title");
/*****
$script_file_name = str_replace('\\', '/', __FILE__);
$script_file_name = substr($script_file_name, strrpos($script_file_name, '/') + 1);
*******/
if ( !(isset($_SESSION['login'])) ){
    $_SESSION = array();
    $_SESSION = array();

    // Note: セッション情報だけでなくセッションを破壊する。
    if (isset($_COOKIE[session_name()])) {
        setcookie(session_name(), '', time()-42000, '/');
    }
    // 最終的に、セッションを破壊する
    session_destroy();
    header("HTTP/1.1 301 Moved Permanently");
    header("Location: index.php");
    exit();
}
if (time() >= ($_SESSION['logintime'] + (60*60)) ){
    $_SESSION = array();
    // 1時間以上だったら
    $_SESSION = array();

    // Note: セッション情報だけでなくセッションを破壊する。
    if (isset($_COOKIE[session_name()])) {
        setcookie(session_name(), '', time()-42000, '/');
    }
    // 最終的に、セッションを破壊する
    session_destroy();
    header("HTTP/1.1 301 Moved Permanently");
    header("Location: index.php");
    exit();
}
$g_Dbh = new DBManager();

// Server URL
$BaseUrl	= "https://" .$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$BaseUrl	= substr($BaseUrl,0,strrpos($BaseUrl, '/')+1);
?>
