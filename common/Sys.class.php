<?php
/**
 * 設定ファイルconfig.iniを読み込む。
 *
 */
class Sys {
  /** @var string */
  var $sysname;
  var $domain;

  /**
   * Initialize
   */
  function Sys($confPath = "./inc/config.ini") {
    $this->propertyList = parse_ini_file($confPath, true);
    if ($this->propertyList == null) {
      die("Cannot Open Config.ini");
    }
	}
  function init() {
    $this->sysname = $this->getProperty("system", "title");
    $this->domain = $this->getProperty("system", "domain");
  }

  /**
   * getProperty
   * @access public
   * @param string $section
   * @param string $name
   */
  function getProperty($section, $key = null) {
    if (!empty($this->propertyList[$section]) && empty($key)) {
      return $this->propertyList[$section];
    }
    else if (!empty($this->propertyList[$section][$key])) {
      return $this->propertyList[$section][$key];
    }
    else {
      return null;
    }
  }

  /**
   * getSysname
   * @access public
   * @return string
   */
  function getSysname() {
    return $this->sysname;
  }
  /**
   * getDomain
   * @access public
   * @return string
   */
  function getDomain() {
    return $this->domain;
  }
}
?>
