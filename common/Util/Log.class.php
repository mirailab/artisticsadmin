<?php
class Log{
  // ログ出力関数 INFOログを出力する
  public static function info($text){
    self::write($text, "INFO");
  }

  // ログ出力関数 ERRORログを出力する
  public static function error($text){
    self::write($text, "ERROR");
  }
  // /log/log-年月日.log ファイルを出力する
  private static function write($text, $log_type){
    $datetime = self::getDateTime();
    $date = self::getDate();
    $file_name = __DIR__ . "/log/log-{$date}.log";
    $text = "{$datetime} [{$log_type}] {$text}" . PHP_EOL;
    return error_log(print_r($text, TRUE), 3, $file_name);
  }
  // 日付を返す(ファイル名用)
  private static function getDate(){
    return date('Ymd');
  }
  // 日時を返す(出力ログ用)
  private static function getDateTime(){
    $datetime = explode(".", microtime(true));
    $date = date('Y-m-d H:i:s', $datetime[0]);
    $time = $datetime[1];
    return "{$date}.{$time}";
  }
}
