<?php
/**
 * Options.class.php
 */
class Options {
	// 選択ボックスのオプション作成
	static function selectOption($items, $default){
		foreach ($items as $key => $value){
			$selected = "";
			if ($default == $key )
				$selected = " selected ";
			printf("<option value='%s' %s>%s</option>", $key, $selected, $value);
		}
	}
	// $start〜$endのオプション
	static function selectCountOption($start, $end){
		for($i=$start; $i<=$end; $i++){
			$selected = "";
			if ($default == $i )
				$selected = " selected ";
			printf("<option value='%d' %s>%-2d</option>", $i, $selected, $i);
		}
	}
	// YYYY/MM/DDのオプション　年は前年から翌年
	static function selectDate($label, $def_date) {
	    @list($yyyy, $mm, $dd) = explode("/", date("Y/m/d"));
	    $now_yyyy = $yyyy;
	    $now_mm = $mm;
	    $now_dd = $dd;

	    if ($def_date != "" ) {
				@list($yyyy, $mm, $dd) = preg_split("/[-\/]/", $def_date);
	    }
	    // 年
	    echo "<select name=\"".$label."yyyy\">";
	    for ($i = $now_yyyy-1; $i <= $now_yyyy+1; $i++) {
	        echo "<option";
	        if ($i == $yyyy) {
        		echo " selected ";
					}
	        echo ">$i";
	    }
	    echo "</select>年";

	   	echo "<select name=\"".$label."mm\">";
	    for ($i = 1; $i <= 12; $i++) {
	        echo "<option";
	        if ($i == $mm) {
						echo " selected ";
	        }
	        echo ">$i";
	    }
	    echo "</select>月";
	    echo "<select name=\"".$label."dd\">";

	    for ($i = 1; $i <= 31; $i++) {
	        echo "<option";
	        if ($i == $dd) {
        		echo " selected ";
	        }
	        echo ">$i";
	    }
	    echo "</select>日";
	}
  /**
   * getYear
   * @access public
   * @param int $from
   * @param int $to
   * @return array
   */
  static function getYear($from, $to) {
    return Options::_getLiner1Array($from, $to);
  }
	static function selectYear($from, $to, $default=0){
		$array = Options::_getLiner1Array($from, $to);
		foreach ($array as $key => $value){
			$selected = "";
			if ($default == $value )
				$selected = " selected ";
			printf("<option value='%s' %s>%s</option>", $value, $selected, $value);
		}
	}
  /**
   * getMonth
   * @access public
   * @return array
   */
  static function getMonth() {
    return Options::_getLiner1Array(1, 12);
  }
  /**
   * selectMonth
   * @access public
   * @return voie
   */
  static function selectMonth($default) {
    $array = Options::_getLiner1Array(1, 12);
		foreach ($array as $key => $value){
			$selected = "";
			if ($default == $value )
				$selected = " selected ";
			printf("<option value='%s' %s>%s</option>", $value, $selected, $value);
		}
  }
  /**
   * getDay
   * @access public
   * @return array
   */
  static function getDay($dd = 31) {
    return Options::_getLiner1Array(1, $dd);
  }

	static function selectDay($default) {
    $array = Options::_getLiner1Array(1, 31);
		foreach ($array as $key => $value){
			$selected = "";
			if ($default == $value )
				$selected = " selected ";
			printf("<option value='%s' %s>%s</option>", $value, $selected, $value);
		}
  }
  /**
   * getUmu
   * @access public
   * @return array
   */
  static function getUmu() {
    return array("f" => "有り", "t" => "無し");
  }
  /**
   * _getLiner1Array
   * @access protected
   * @param int $from
   * @param int $to
   * @return array
   */
  static function _getLiner1Array($from, $to) {
    $opts = array();
    for ($i = $from; $i <= $to; $i++) {
      $opts[] = $i;
    }
    return $opts;
  }
}
?>
