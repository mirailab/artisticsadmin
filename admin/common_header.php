<?php
/**
* common_header.php
* ページ共通ヘッダー部　左メニュー＆上部バー
*
*
**/
$g_AdminName = "管理者名";
$g_AdminCompany = "管理会社名";
?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="robots" content="NOINDEX,NOFOLLOW" />
<!--
// favisonの設定はここに
  <link rel="shortcut icon" href="favicon.ico?f=1566968868" type="image/vnd.microsoft.icon">
<link rel="icon" href="favicon.ico?f=1566968868" type="image/vnd.microsoft.icon">
-->
  <title><?= $g_Title ?></title>
  <!-- Font Awesome Icons -->
  <link href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  <link rel="stylesheet" href="../dist/css/adminlte.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style>
  p.submenu
    {
       padding-left: 2em;
    }
  </style>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
    </ul>
    <!-- SEARCH FORM -->
<!--
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
    </form>
-->
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
    <li class="nav-item">
    <a class="nav-link" href="./logout.php">
      ログアウト<i class="nav-icon fas fa-sign-out-alt"></i></a>
    </li>
  </ul>
  </nav>
  <!-- /.navbar -->
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="toppage.php" class="brand-link">
      <span class="brand-text font-weight-light"><?= $g_Title ?></span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
          <a href="#" class="d-block"><?= $g_AdminName ?></a>
          <small class="text-muted"><?= $g_AdminCompany ?></small>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
         <li class="nav-item">
           <a href="top.php" class="nav-link">
             <i class="nav-icon fas fa-home"></i>
             <p>
               Top
             </p>
           </a>
         </li>
         <li class="nav-item has-treeview">
           <a href="#" class="nav-link">
             <i class="nav-icon fa fa-bullhorn"></i>
             <p>
               お知らせ管理
               <i class="fa fa-angle-left right"></i>
             </p>
           </a>
           <ul class="nav nav-treeview">
             <li class="nav-item">
               <a class="nav-link" href="#">
                 <p class="submenu">通知送信</p>
               </a>
             </li>
             <li class="nav-item">
               <a class="nav-link" href="#">
                 <p class="submenu">通知履歴</p>
               </a>
             </li>
           </ul>
         </li>
         <li class="nav-item has-treeview">
           <a href="#" class="nav-link">
             <i class="nav-icon fas fa-users"></i>
             <p>
               会員管理
             <i class="right fa fa-angle-left"></i>
             </p>
           </a>
           <ul class="nav nav-treeview">
             <li class="nav-item">
               <a class="nav-link" href="user_ctl.php">
                 <p class="submenu">会員一覧</p>
               </a>
             </li>
<!--
             <li class="nav-item">
               <a class="nav-link" href="member_ctl.php">
                 <p class="submenu">新規登録</p>
               </a>
             </li>
-->
           </ul>
         </li>
         <li class="nav-item has-treeview">
           <a href="#" class="nav-link">
             <i class="nav-icon fas fa-stream"></i>
             <p>
               タイムライン管理
               <i class="fa fa-angle-left right"></i>
             </p>
           </a>
           <ul class="nav nav-treeview">
             <li class="nav-item">
               <a class="nav-link" href="#">
                 <p class="submenu">タイムライン一覧</p>
               </a>
             </li>
           </ul>
         </li>
         <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-rss"></i>
              <p>
                RSS管理
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <p class="submenu">RSS一覧</p>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <p class="submenu">新規登録</p>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <p class="submenu">ニュースコメント一覧</p>
                </a>
              </li>
            </ul>
        </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-comment-alt"></i>
              <p>
                定型メッセージ管理
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <p class="submenu">通常のお問い合わせ</p>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <p class="submenu">保険関連</p>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <p class="submenu">家屋調査</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
<?php
 ?>
