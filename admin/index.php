<?php
/**
* index.php
* ログイン処理
*
* @access public
* @param array
*  login.phpから渡されるPOSTパラメータ
* 　"mode"=>LOGIN,
* 　"password"=>XXXX,
*
* @return
*
**/
ini_set("include_path", "./:../:./inc/:../inc:./common/:../common:./Classes:../Classes:../:./lib:../lib:../lib/PEAR:../logic:/usr/share/php:" . ini_get("include_path"));
mb_language('ja');
ini_set('mbstring.internal_encoding', 'UTF8');
ini_set('mbstring.detect_order', 'auto');
ini_set('mbstring.http_input', 'auto');
ini_set('mbstring.http_output', 'UTF8');
ini_set('session.gc_maxlifetime', 86400 );  // 秒(デフォルト:1440)
ini_set('error_reporting', E_ERROR);
@session_destroy();
@session_start();

$_SESSION = array();

require_once("Sys.class.php");

$gSys = new Sys("./conf/config.ini");
$gSys->init();

$loginId = $gSys->getProperty("admin", "login");
$pass = $gSys->getProperty("admin", "password");

//echo "lodinId=>$loginId<br>";
//require_once("DBManager.class.php");
include_once("Util/CGIUtil.class.php");

$g_Title = $gSys->getProperty("system", "title");
// DB接続
// mysql:host=ホスト名;dbname=データベース名;charset=文字エンコード
//$dsn = 'mysql:host=mysql705.db.sakura.ne.jp;dbname=hotel-concierge_master;charset=utf8';
/******************
$g_DBinfo = $gSys->getProperty("db", "");
//var_dump($g_DBinfo);
$dbh = new DBManager($g_DBinfo);
***************/
$_errormessage = "";
if (isset($_POST['mode']) && $_POST['mode'] == "LOGIN"){
  $form = CGIUtil::getFormParm();
  extract($form, EXTR_OVERWRITE);
/*******
  print "login  =>$login<br>";
  print "loginId=>$loginId<br>";
  print "password  =>$password<br>";
  print "pass      =>$pass<br>";
********/
  if (!strcmp($login, $loginId)){
    $auth = password_verify($password, $pass);
    if ($auth) {
      $_SESSION['login'] = $login;
      $_SESSION['logintime'] = time();  // ログイン時間を保存
      header("HTTP/1.1 301 Moved Permanently");
      header("Location: top.php");
      exit();
    }else {
      $_errormessage = "パスワードが一致しません";
    }
  }else {
    $_errormessage = "メールアドレスが一致しません";
  }
}
include_once("login.php");
?>
