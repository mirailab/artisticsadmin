<?php
require_once("../inc/admin.inc.php");
ini_set( 'error_reporting', E_ALL);
require_once("../inc/DB_UserMasterTblManager.class.php");

include_once("../common/Util/CGIUtil.class.php");
include_once("../common/Util/Util.class.php");
include_once("../common/Util/DateUtil.class.php");
include_once("../common/Util/StrUtil.class.php");
include_once("../common/Util/Common.func.php");

$_errorMessage = $_completeMessage = "";

# POSTされたデータをチェック
$form = CGIUtil::getFormParm();
# エラーメッセージを保存する配列を初期化
$error = array();
$userDbh = new DB_UserMasterTblManager();
//Util::_var_dump($form);
if (isset($form['mode'])){
  switch($form['mode']){
  // モード別に登録・更新・削除などの処理をここに書く
    default:
      break;
  }
}
require_once("userlist.php");
exit();
?>
