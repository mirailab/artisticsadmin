<?php
const MAX_SIZE = 10240000;    // 最大ファイルサイズ 1M
// php.iniのpost_max_sizeを超えたデータが送信されていないかチェックする関数
function checkPostMaxSize()
{
  $max_size = ini_get('post_max_size');
  //print "max_size=>$max_size<br>";
  // post_max_sizeが8Mのように設定されていた場合に整数にする
  $multiple = 1;
  $unit = substr($max_size, -1);
  if ($unit == 'M') {
    $multiple = 1024 * 1024;
  } elseif ($unit == 'K') {
    $multiple = 1024;
  } elseif ($unit == 'G') {
    $multiple = 1024 * 1024 * 1024;
  }
  $max_size = substr($max_size, 0, strlen($max_size) - 1) * $multiple;

  // post_max_sizeを超えたデータがPOSTされたかどうかチェック
  if ($_SERVER['REQUEST_METHOD'] == 'POST' &&
      $_SERVER['CONTENT_LENGTH'] > $max_size) {
      return false;
    } else {
      return true;
  }
}
function checkFileForUploadfile()  // アップロードファイルをチェックする関数
{
  $error_msg = array();
  $ext       = '';

  $size     = $_FILES['uploadfile']['size'];
  $error    = $_FILES['uploadfile']['error'];
  $img_type = $_FILES['uploadfile']['type'];
  $tmp_name = $_FILES['uploadfile']['tmp_name'];
  $finfo = new finfo();
  $img_type = $finfo->file($_FILES['uploadfile']['tmp_name'], FILEINFO_MIME_TYPE);
  //print "img_type=>$img_type\n";

  if ($error != UPLOAD_ERR_OK) {  // アップロードエラーの場合
    if ($error == UPLOAD_ERR_NO_FILE) {
      // アップロードされなかった場合はエラー処理を行なわない
    } elseif ($error == UPLOAD_ERR_INI_SIZE ||
              $error == UPLOAD_ERR_FORM_SIZE) { // ファイルサイズエラー
      $error_msg[] = 'ファイルサイズは10M以下にしてください';
    } else {  // その他のエラーの場合
      $error_msg[] = 'アップロードエラーです';
    }
    return array(false, $ext, $error_msg);
  } else {  // アップロードエラーでない場合
    // 送信されたMIMEタイプから拡張子を決定
    if ($img_type == 'image/gif') {
      $ext = 'gif';
    } elseif ($img_type == 'image/jpeg' || $img_type == 'image/pjpeg') {
      $ext = 'jpg';
    } elseif ($img_type == 'image/png' || $img_type == 'image/x-png') {
      $ext = 'png';
    }

# 画像ファイルのMIMEタイプを判定
    $finfo = new finfo(FILEINFO_MIME_TYPE);
    $finfoType = $finfo->file($tmp_name);

# 画像ファイルのサイズ下限をチェックします。
    if ($size == 0) {
      $error_msg[] = 'ファイルが存在しないか空のファイルです';
# 画像ファイルのサイズ上限をチェックします。
    } elseif ($size > MAX_SIZE) {
      $error_msg[] = 'ファイルサイズは1M以下にしてください';
# 送信されたMIMEタイプと、画像ファイルのMIMEタイプが一致するかを確認します。
    } elseif ($img_type != $finfoType) {
      $error_msg[] = 'MIMEタイプが一致しません';
# 画像ファイルの拡張子をチェックします。
    } elseif ($ext != 'gif' && $ext != 'jpg' && $ext != 'png') {
      $error_msg[] = 'アップロード可能なファイルはgif、jpg、pngのみです';
    } else {
      return array(true, $ext, $error_msg);
    }
  }
  return array(false, $ext, $error_msg);
}
function checkFileForImagefile()  // アップロードファイルをチェックする関数
{
  $error_msg = array();
  $ext       = '';

  $size     = $_FILES['imagefile']['size'];
  $error    = $_FILES['imagefile']['error'];
  $img_type = $_FILES['imagefile']['type'];
  $tmp_name = $_FILES['imagefile']['tmp_name'];

  $finfo = new finfo();
  //var_dump($finfo);

  $img_type = $finfo->file($_FILES['imagefile']['tmp_name'], FILEINFO_MIME_TYPE);
  //print "img_type=>$img_type<br>";

  if ($error != UPLOAD_ERR_OK) {  // アップロードエラーの場合
    if ($error == UPLOAD_ERR_NO_FILE) {
      // アップロードされなかった場合はエラー処理を行なわない
      $error_msg[] = 'UPLOAD_ERR_NO_FILE';

    } elseif ($error == UPLOAD_ERR_INI_SIZE ||
              $error == UPLOAD_ERR_FORM_SIZE) { // ファイルサイズエラー
          //print "Error=>$error<br>";
          //print "file size error=><br>";
      $error_msg[] = 'ファイルサイズは10M以下にしてください';
    } else {  // その他のエラーの場合
      $error_msg[] = 'アップロードエラーです';
    }
    return array(false, $ext, $error_msg);
  } else {  // アップロードエラーでない場合
    // 送信されたMIMEタイプから拡張子を決定
    if ($img_type == 'image/gif') {
      $ext = 'gif';
    } elseif ($img_type == 'image/jpeg' || $img_type == 'image/pjpeg') {
      $ext = 'jpg';
    } elseif ($img_type == 'image/png' || $img_type == 'image/x-png') {
      $ext = 'png';
    } else {
      $error_msg = sprintf("ファイル拡張子エラー[%s]", $img_type);
      return array(false, $ext, $error_msg);
    }

# 画像ファイルのMIMEタイプを判定
    $finfo = new finfo(FILEINFO_MIME_TYPE);
    $finfoType = $finfo->file($tmp_name);

# 画像ファイルのサイズ下限をチェックします。
    if ($size == 0) {
      $error_msg[] = 'ファイルが存在しないか空のファイルです';
# 画像ファイルのサイズ上限をチェックします。
    } elseif ($size > MAX_SIZE) {
      print "Size Error=><br>";
      $error_msg[] = 'ファイルサイズは1M以下にしてください';
# 送信されたMIMEタイプと、画像ファイルのMIMEタイプが一致するかを確認します。
    } elseif ($img_type != $finfoType) {
      $error_msg[] = 'MIMEタイプが一致しません';
# 画像ファイルの拡張子をチェックします。
    } elseif ($ext != 'gif' && $ext != 'jpg' && $ext != 'png') {
      $error_msg[] = 'アップロード可能なファイルはgif、jpg、pngのみです';
    } else {
      return array(true, $ext, $error_msg);
    }
  }
  return array(false, $ext, $error_msg);
}
/**
* @param int current_url_path 現在のパス
*/
function current_url_path() {
	$ht=empty($_SERVER['HTTPS']) ? "http://" : "https://";
	$host=$_SERVER['HTTP_HOST'];
	$uri=$_SERVER['REQUEST_URI'];
	$uri = str_replace(basename($uri), "",$uri);
	$path=$ht .$host .$uri;
	return $path;
}

function checkFormInput($var)
{
	if (is_array($var)) {
		return array_map('checkFormInput', $var);
	} else {
		# magic_quotes_gpcがOnでもOffでも動作するようにする
		if (get_magic_quotes_gpc()) {
			$var = stripslashes($var);
		}
		# nullバイトを含む制御文字が含まれていないかをチェック（最大1000文字）。
		if (preg_match('/\A[\r\n\t[:^cntrl:]]{0,1000}\z/u', $var) == 0) {
			return -1;
		}
		# 文字エンコードの確認
		if (! mb_check_encoding($var, 'UTF-8')) {
			return -1;
		}
		return $var;
	}
}
function html_escape($var)  // HTMLでのエスケープ処理をする
{
	if (is_array($var)) {
		return array_map('html_escape', $var);
	} else {
		return htmlspecialchars($var, ENT_QUOTES, 'UTF-8');
	}
}
function checkEmail($email) {
	$ok =
			ereg("^[0-9,A-Z,a-z][0-9,a-z,A-Z,_,\.,-]+@"
			. "[0-9,A-Z,a-z][0-9,a-z,A-Z,_,\.,-]+\."
			. "(af|al|dz|as|ad|ao|ai|aq|ag|ar|am|aw|ac|au|at|az|"
			. "bh|bd|bb|by|bj|bm|bt|bo|ba|bw|br|io|bn|bg|bf|bi|kh|"
			. "cm|ca|cv|cf|td|gg|je|cl|cn|cx|cc|co|km|cg|cd|ck|cr|"
			. "ci|hr|cu|cy|cz|dk|dj|dm|do|tp|ec|eg|sv|gq|er|ee|et|"
			. "fk|fo|fj|fi|fr|gf|pf|tf|fx|ga|gm|ge|de|gh|gi|gd|gp|gu|"
			. "gt|gn|gw|gy|ht|hm|hn|hk|hu|is|in|id|ir|iq|ie|im|il|it|"
			. "jm|jo|kz|ke|ki|kp|kr|kw|kg|la|lv|lb|ls|lr|ly|li|lt|lu|"
			. "mo|mk|mg|mw|my|mv|ml|mt|mh|mq|mr|mu|yt|mx|fm|md|mc|mn|"
			. "ms|ma|mz|mm|na|nr|np|nl|an|nc|nz|ni|ne|ng|nu|nf|mp|no|"
			. "om|pk|pw|pa|pg|py|pe|ph|pn|pl|pt|pr|qa|re|ro|ru|rw|kn|"
			. "lc|vc|ws|sm|st|sa|sn|sc|sl|sg|sk|si|sb|so|za|gs|es|lk|"
			. "sh|pm|sd|sr|sj|sz|se|ch|sy|tw|tj|tz|th|bs|ky|tg|tk|to|"
			. "tt|tn|tr|tm|tc|tv|ug|ua|ae|uk|us|um|uy|uz|vu|va|ve|vn|"
			. "vg|vi|wf|eh|ye|yu|zm|zw|"
			. "com|net|org|gov|edu|int|mil|biz|info|name|pro|jp)"
			. "$",$email);
	return $ok;
}

/**
 * "stringUtil"�Τ褦��ʸ�����"string_util"���Ѵ�����
 * @access public static
 * @param string $str
 * @return string
 */
function uc2us($str) {
	/* ʸ����������ʬ�� */
	$chars = StrUtil::str2array($str);
	$ret = array();
	$first = true;
	foreach ($chars as $char) {
		if (($char >= "A") && ($char <= "Z")) {
			/* ��ʸ������ե��٥åȤ��ä��� */
			if ($first == false) {
				/* ��Ƭ����ʤ����_���ɲ� */
				$ret[] = "_";
			}
			/* ��ʸ���ˤ��� */
			$ret[] = strtolower($char);
		}
		else {
			$ret[] = $char;
		}
		$first = false;
	}

	return join("", $ret);
}

/**
 * "string_util"�Τ褦��ʸ�����"stringUtil"���Ѵ�����"
 * @access public
 * @param string $str
 * @return string
 */
function us2uc($str) {
	/* ʸ����������ʬ�� */
	$chars = StrUtil::str2array($str);
	$ret = array();
	$uc = false;
	foreach ($chars as $char) {
		if ($char == "_") {
			$uc = true;
		}
		else if ($uc) {
			$ret[] = strtoupper($char);
			$uc = false;
		}
		else {
			$ret[] = $char;
		}
	}
	var_dump($ret);
	return join("", $ret);
}

/**
 * ʸ�����ʸ���������ʬ�򤹤�
 * @access public
 * @param string $str
 * @return array
 */
function str2array($str) {
	$length = strlen($str);
	$chars = array();
	for ($i = 0; $i < $length; $i++) {
		$chars[$i] = $str{$i};
	}
	return $chars;
}

/**
 * Ⱦ�ѥ��ʤ����Ѥ��Ѵ�
 * @access public static
 * @param string $str
 * @return string
 */
function convertHanZenKana($str) {
	return mb_convert_kana($str);
}

/**
 * ���ѱѿ����Ⱦ�Ѥ��Ѵ�
 * @access public static
 * @param string $str
 * @return string
 */
function convertZenHanEiNum($str) {
	return mb_convert_kana($str, "a");
}

/**
 * ��2�Ĥ���٤��Ѵ�
 * @access public
 * @param string
 * @return string
 */
function correctZenHan($str) {
	return StrUtil::convertZenHanEiNum(StrUtil::convertHanZenKana($str));
}

/**
 * �Ҥ餬�ʤ�����������å�����
 * @access public static
 * @param string $str
 * @return true:�Ҥ餬�ʤ��� false:�Ҥ餬�ʤ�������ʤ�
 */
function isOnlyHiragana($str) {
	return mb_ereg("[^��-��]", $str) === false;
}

/**
 * �������ʤ�����������å�����
 * @access public static
 * @param string $str
 * @return true:�������ʤ��� false:�������ʤ�������ʤ�
 */
function isOnlyKatakana($str) {
	return mb_ereg("[^��-����0-9�� ]", $str) === false;
}

/**
 * �ѿ��������������å�����
 * @access public static
 * @param string $str
 * @return true:�ѿ������ false:�ѿ����������ʤ�
 */
function isOnlyEisu($str) {
	return mb_ereg("[^a-zA-Z0-9]", $str) === false;
}

/**
 * URL�ǻȤ���ʸ��������������å�����
 * @access public static
 * @param string $str
 * @return true:OK false:NG
 */
function isUrl($str) {
	return mb_ereg("[^-_$.+!*'(),a-zA-Z0-9]", $str) === false;
}

/**
 * ����ȥϥ��ե������������å�����
 * @access public static
 * @param string $str
 * @return true:OK false:NG
 */
function isNumHyphen($str) {
	return mb_ereg("[^-0-9]", $str) === false;
}

/**
 * �ѿ���ȥϥ��ե�ȥ������������������������å�����
 * @access public
 * @param string $str
 * @return true:OK false:NG
 */
function isEisuHyphUscore($str) {
	return mb_ereg("[^-_a-zA-Z0-9]", $str) === false;
}

/**
 * ʸ�����Ŭ����Ĺ���Ǿ�ά����
 * @access public
 * @param string $str
 * @param int $len
 * @param string $tail
 */
function shrink($str, $len, $tail = "...") {
	if (mb_strlen($str) > $len) {
		return mb_substr($str, 0, $len - 1) . $tail;
	}
	else {
		return $str;
	}
}
function convert_kigo_han_zen($str, $option) {
	// option=0:半角から全角, 1:全角から半角
	$pairs = array(
			array(" ", "　"), // 半角スペース⇔全角スペース
			array("!", "！"),
			array("#", "＃"),
			array("$", "＄"),
			array("%", "％"),
			array("&", "＆"),
			array("'", "’"),
			array("(", "（"),
			array(")", "）"),
			array("*", "＊"),
			array("+", "＋"),
			array(",", "，"),
			array("-", "－"),
			array(".", "．"),
			array("/", "／"),
			array(":", "："),
			array(";", "；"),
			array("<", "＜"),
			array("=", "＝"),
			array(">", "＞"),
			array("?", "？"),
			array("@", "＠"),
			array("[", "［"),
			array("]", "］"),
			array("^", "＾"),
			array("_", "＿"),
			array("`", "｀"),
			array("{", "｛"),
			array("|", "｜"),
			array("}", "｝"),
			array("~", "～"),
			array("｡", "。"),
			array("｢", "「"),
			array("｣", "」"),
			array("､", "、"),
			array("･", "・")
	);
	foreach($pairs as $pair) {
			$before = $pair[$option];
			$after = $pair[1 - $option];
			$str = str_replace($before, $after, $str);
	}
	return $str;
}
?>
