<?php
date_default_timezone_set('Asia/Tokyo');
/**
 * DateUtilクラス
 * --------------------------------------------------------
 */
class DateUtil {
  /**
   * DateUtil::split
   * "yyyy/mm/dd"をdate["y"],date["m"],date["d"]に変換
   * @access public
   * @param string $date
   */
  static function split($date) {
    list($y, $m, $d) = preg_split("/[-\/]/", $date);
	$m=sprintf("%d", $m);
	$d=sprintf("%d", $d);
    return array("y" => $y, "m" => $m, "d" => $d);
  }
  /**
   * DateJoin
   * date["y"],date["m"],date["d"]を"yyyy-mm-dd"に変換
   * @access public
   * @param array $date
   */
  static function join($date) {
    return join("/", $date);
  }

  /**
   * 所定期間内の所定曜日の日付データを取得する
   * @access public
   * @param string $from "yyyy-mm-dd"
   * @param string $to "yyyy-mm-dd"
   * @param int $wday 0:sun 6:sat
   */
  static function getDatesEveryWeek($from, $to, $wday) {
    $time = DateUtil::mktime($from);
    list($y, $m, $d) = explode("-", $from);
    $toT = DateUtil::mktime($to);

    $ret = array();
    $i = 0;

    while ($time <= $toT) {
      $gd_ = getdate($time);
      if ($gd_["wday"] == $wday) {
        $ret[] = date("Y-m-d", $time);
      }

      $i++;
      $time = mktime(0, 0, 0, $m, $d + $i, $y);
    }

    return $ret;
  }
  /**
   * 検索用に日付データを取得する
   * @access public
   * @param string $from "yyyy-mm-dd"
   * @param string $to "yyyy-mm-dd"
   */
  static function getSearchDates($from, $to) {
	$from = str_replace("/", "-", $from);
	$to = str_replace("/", "-", $to);

    $time = DateUtil::mktime($from);
   	$date = DateUtil::split($from);
	$y=$date['y'];
	$m=$date['m'];
	$d=$date['d'];

    $toT = DateUtil::mktime($to);
    $ret = array();
    $i = 0;
    while ($time <= $toT) {
      $date = $ret[$i] = date("Y-m-d", $time);

      $i++;
      $time = mktime(0, 0, 0, $m, $d + $i, $y);
    }
    return $ret;
  }

  /**
   * 検索用曜日データを初期化する
   * @access private
   * @param array $wday
   */
  static function wdayInit($wday) {
    $ret = array();

    if (isset($wday["sun"]) && ($wday["sun"] == 1)) {
      $ret[] = 0;
    }

    if (isset($wday["mon"]) && ($wday["mon"] == 1)) {
      $ret[] = 1;
    }

    if (isset($wday["tue"]) && ($wday["tue"] == 1)) {
      $ret[] = 2;
    }

    if (isset($wday["wed"]) && ($wday["wed"] == 1)) {
      $ret[] = 3;
    }

    if (isset($wday["thu"]) && ($wday["thu"] == 1)) {
      $ret[] = 4;
    }

    if (isset($wday["fri"]) && ($wday["fri"] == 1)) {
      $ret[] = 5;
    }

    if (isset($wday["sat"]) && ($wday["sat"] == 1)) {
      $ret[] = 6;
    }

    return $ret;
  }

  /**
   * "yyyy-mm-dd"をunix時間に変換
   * @access public
   * @param string $date
   */
  static function mktime($date) {
    $d = DateUtil::split($date);
//print "mktime=>$date<br>\r\n";
//print_r($d);
  $month = sprintf("%d", $d['m']);
  $day = sprintf("%d", $d['d']);
  $year = sprintf("%d", $d['y']);

  //print "month=>$month<br>\r\n";
  //print "day=>$day<br>\r\n";
  //print "year=>$year<br>\r\n";

    return mktime(0, 0, 0, $month, $day, $year);
  }
  /**
   * "yyyy-mm-dd"から曜日を取得する
   * @access public
   * @param string $date
   */
  static function getWday($date) {
    $gd = getdate(DateUtil::mktime($date));
    return $gd["wday"];
  }

  /**
   * "yyyy-mm-dd"から曜日を取得する
   * 0:日 6:土
   * @access public
   * @param string $date
   */
  static function getWdayJp($date) {
    $wday = DateUtil::getWday($date);
    switch ($wday) {
      case 0: $ret = "日"; break;
      case 1: $ret = "月"; break;
      case 2: $ret = "火"; break;
      case 3: $ret = "水"; break;
      case 4: $ret = "木"; break;
      case 5: $ret = "金"; break;
      case 6: $ret = "土"; break;
      default: $ret = "日";
    }
    return $ret;
  }

  /**
   * 日をオフセットする
   * @access public
   * @param string $date 日付 yyyy-mm-dd
   * @param int $offset オフセットする日数
   * @return string オフセット後の日付
   */
  static function offsetDay($date, $offset) {
    $d = DateUtil::split($date);
    return date("Y-m-d", @mktime(0, 0, 0, $d["m"], $d["d"] + $offset, $d["y"]));
  }

  /**
   * 月をオフセットする
   * @access public
   * @param string $date 日付 yyyy-mm-dd
   * @param int $offset オフセットする月数
   * @return string オフセット後の日付
   */
  static function offsetMonth($date, $offset) {
    $d = DateUtil::split($date);
    return date("Y/m/d", mktime(0, 0, 0, $d["m"] + $offset, $d["d"], $d["y"]));
  }

  /**
   * 1週間前の日付を取得する
   * @access public
   * @param string $date 日付 yyyy-mm-dd
   * @return string dateの1週間前の日付
   */
  static function getPrevWeek($date) {
    return DateUtil::offsetDay($date, -7);
  }

  /**
   * 1日前の日付を取得する
   * @access public
   * @param string $date 日付 yyyy-mm-dd
   * @return string dateの1日前の日付
   */
  static function getPrevDay($date) {
    return DateUtil::offsetDay($date, -1);
  }

  /**
   * 1日後の日付を取得する
   * @access public
   * @param string $date 日付 yyyy-mm-dd
   * @return string dateの1日後の日付
   */
  static function getNextDay($date) {
    return DateUtil::offsetDay($date, 1);
  }

  /**
   * 1週間後の日付を取得する
   * @access public
   * @param string $date 日付 yyyy-mm-dd
   * @return string dateの1週間後の日付
   */
  static function getNextWeek($date) {
    return DateUtil::offsetDay($date, 7);
  }

  /**
   * 1ヵ月後の日付を取得する
   * @access public
   * @param string $date
   * @return string
   */
  static function getNextMonth($date) {
    return DateUtil::offsetMonth($date, 1);
  }

  /**
   * 1ヶ月前の日付を取得する
   * @access public
   * @param string $date
   * @return string
   */
  static function getPrevMonth($date) {
    return DateUtil::offsetMonth($date, -1);
  }

  /**
   * 日付の差を取得する
   * @access public
   * @param string $d1 日付
   * @param string $d2 日付
   * @return string 日付の差 d1が大きいと正、d2が大きいと負の日数
   */
  static function compare($d1, $d2) {
#print "compare 1=>".$d1."<br>\r\n";
#print "compare 2=>".$d2."<br>\r\n";
    return (DateUtil::mktime($d1) - DateUtil::mktime($d2)) / 86400;
  }

  /**
   * 1日の日付を返す
   * @access public
   * @param string $date
   * @param int $offset
   * @return string
   */
  static function getMonthBegin($date, $offset = 0) {
    $d = DateUtil::split($date);
    return date("Y/m/d", mktime(0, 0, 0, $d["m"] + $offset, 1, $d["y"]));
  }

  /**
   * 月末の日付を返す
   * @access public
   * @param string $date
   * @param int $offset
   * @return string
   */
  static function getMonthEnd($date, $offset = 0) {
    $d = DateUtil::split($date);
    return date("Y/m/t", mktime(0, 0, 0, $d["m"] + $offset, $d["d"], $d["y"]));
  }

  /**
   * 月末の日を返す
   * @access public
   * @param string $date
   * @param int $offset
   * @return string
   */
  static function getMonthEndD($date, $offset = 0) {
    $d = DateUtil::split($date);
    return date("t", mktime(0, 0, 0, $d["m"] + $offset, $d["d"], $d["y"]));
  }
  /**
   * date関数簡略化
   * @param int $y 年
   * @param int $m 月
   * @param int $d 日
   * @return string yyyy-mm-dd
   */
  static function date($m, $d, $y) {
    return date("Y/m/d", mktime(0, 0, 0, $m, $d, $y));
  }

  /**
   * 日を返す
   * @param string $date
   */
  static function getDay($date) {
    $d = DateUtil::split($date);
    return $d["d"];
  }

  /**
	 * 日付をチェックする
	 * @access public
	 * @param string $date
	 * @return true:正しい false:正しくない
	 */
	static function checkDate($date) {
    if (strlen($date) == 0) { return false; }
		$d = DateUtil::split($date);
		return checkdate($d['m'], $d['d'], $d['y']);
	}

	/**
	 * YYYY-MMを取得する
	 * @access public
	 * @param string $date
	 * @return string
	 */
	static function toYM($date) {
		$d = DateUtil::split($date);
		return $d["y"] . "/" . $d["m"];
	}

  /**
   * 日付をフォーマットする
   * @access public
   * @param string $date
   * @return string
   */
  static function format($date) {
    $day = DateUtil::split($date);
    return sprintf("%s年%d月%d日", $day['y'],$day['m'],$day['d']);
  }
  /**
   * 日付時間(YYYY-MM-DD HH:mm:ss)を曜日付でフォーマットする
   * @access public
   * @param string $date
   * @return string
   */
  static function datetimeformat($date) {
	list($day, $time) = explode(" ", $date);
	$week = DateUtil::getWdayJp($day);
    $dd = DateUtil::split($day);
    return sprintf("%s年%d月%d日(%s) %s", $dd['y'],$dd['m'],$dd['d'], $week, substr($time, 0, strlen("HH:mm")));
  }

  /**
   * 所定の日付より期首(4/1)の日付を求める
   * @access public
   * @param string $date
   * @return string
   */
  static function getBeggingOfPeriod($date){
	$d=DateUtil::split($date);
	$year = $d['m'] < 4 ? $d['y']-1 : $d['y'];
	$bop = sprintf("%s/04/01",$year);
	return $bop;
  }

  /**
   * 所定の期間の曜日別の日数を求める
   * @access public
   * @param string $from
   * @param string $to
   * @return array(0=>日曜日の日数、1=>月曜日の日数...)
   */
  static function getNumOfWeekOfDays($from, $to){
	$ret = array(0,0,0,0,0,0,0);

    $time = DateUtil::mktime($from);
    list($y, $m, $d) = split("/", $from);
    $toT = DateUtil::mktime($to);

    $ret = array();
    $i = 0;

    while ($time <= $toT) {
      $gd_ = getdate($time);
      $week_no=$gd_["wday"];
	  $ret[$week_no] += 1;
      $i++;
      $time = @mktime(0, 0, 0, $m, $d + $i, $y);
    }
    return $ret;
  }
}
?>
