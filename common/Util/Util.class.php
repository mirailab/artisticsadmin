<?php
class Util {
  /**
   * var_dump
   */
  static function _var_dump($var) {
  	Util::_echo("<pre>");
  	var_dump($var);
  	Util::_echo("</pre>");
  }
  /**
   * echo
   */
  static function _echo($var) {
  	echo($var);
  	echo("<br />\n");
  }
}
?>
