<?php
/**
* DBManager
*　DB操作基底クラス
* @package DB
* @author N.Tsuchimoto
*/
require_once("Sys.class.php");

class DBManager{
	/** @var MySql connection */
	private $dbh;

  	function __construct() {
		global $gSys;
		$DBinfo = $gSys->getProperty("db", "");

//var_dump($DBinfo);

		// mysql:host=ホスト名;dbname=データベース名;charset=文字エンコード
        //$dsn = 'mysql:host=mysql705.db.sakura.ne.jp;dbname=hotel-concierge_master;charset=utf8';
        //var_dump($g_DBinfo);
        $g_DBServer = $DBinfo['host'];												// Database Server Name
        $g_DBUser = $DBinfo['user'];														// Database User
        $g_DBPassword = $DBinfo['pass'];													// Database Password
        $g_DBName = $DBinfo['dbname'];

        $dsn = sprintf("mysql:host=%s;dbname=%s;charset=utf8", $g_DBServer, $g_DBName);
        try {
			// PDOインスタンスを生成
            $this->dbh = new PDO($dsn, $g_DBUser, $g_DBPassword);
			$this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        } catch (PDOException $e) {
            // エラー（例外）が発生した時の処理
            // エラーメッセージを表示させる
            echo 'データベースにアクセスできません！' . $e->getMessage();
            // 強制終了
            return;
		}
  	}

  	function __destruct() {
    	$this->dbh = null;
  	}

  	protected function prepare($sql, $driverOptions = array() ) {
    	return $this->dbh->prepare($sql, $driverOptions);
  	}
	public function sql_string($str) {
		if (strlen($str) <= 0)
			return "''";
		//if(!get_magic_quotes_gpc()) {
			$str = addslashes($str);
		//}
		$str = sprintf("'%s'", $str);
		return($str);
	}
	/**
	* getConn
	* @access public
	* @return PDOConnection
	*/
	function getConn() {
		return $this->dbh;
	}

	/**
	* setConn
	* @access public
	* @param PDOConnection $conn
	*/
	function setConn($conn) {
		$this->dbh = $conn;
	}
	/**
	 * _query
	 * @access protected
	 * @param string $stmt
	 * @param string $name
	 * @param boolean $die
	 * @return 成功ならDB_Result
	 */
	protected function query($sql) {
		$result = $this->dbh->query($sql);
		return $result;
	}
	protected function rollback(){
		$this->dbh->getConnrollBack();
	}
	protected function begin(){
		$this->dbh->beginTransaction();
	}
	protected function commit(){
		$this->dbh->commit();
	}
}
?>
