<?php
function generateThumbForJpg($uploadImage, $thumb_name){

  //  ①アップロードした画像のサイズを取得する
  $imagesize=getimagesize($uploadImage);
  //サイズを取得
  $width=$imagesize[0];
  $height=$imagesize[1];

  //サムネイルのサイズ
  $width_s=120;
  $height_s=round($width_s*$height/$width);

  //--------------------------------------------------------
  //  ②imagecreatefromjpeg関数で画像を取り出しサンプリングに使う。
  //--------------------------------------------------------
  $image=imagecreatefromjpeg($uploadImage);

  //--------------------------------------------------------
  //  ③imagecreatetruecolor関数でベースをつくり
  //    そこにサンプリングしてコピーする
  //--------------------------------------------------------
   $image_s=imagecreatetruecolor($width_s, $height_s);
   //サンプリングする
   $result=imagecopyresampled($image_s,$image,0,0,0,0,$width_s,$height_s,$width,$height);
  //--------------------------------------------------------
  //  ④imagejpg関数でブラウザあるいはファイルとして出力する
  //--------------------------------------------------------
  $status = 0;
  if($result){ //サンプリングによる画像作成成功
    if(imagejpeg($image_s,$thumb_name)){ //imagejpeg関数で画像を保存
      $status = 0;
    }else{
      $status -1;
    }
  }else{
    $status = -1;
  }
 //取り出した元画像とサンプリングのための作成したベースが画像を削除する
 imagedestroy($image);
 imagedestroy($image_s);
 return $status;
}
function generateThumbForPng($uploadImage, $thumb_name){
  //print "uploadImage=>$uploadImage<br>";

  //  ①アップロードした画像のサイズを取得する
  $imagesize=getimagesize($uploadImage);
  //サイズを取得
  $width=$imagesize[0];
  $height=$imagesize[1];

  //サムネイルのサイズ
  $width_s=120;
  $height_s=round($width_s*$height/$width);

  //--------------------------------------------------------
  //  ②imagecreatefrompng関数で画像を取り出しサンプリングに使う。
  //--------------------------------------------------------
  $image=imagecreatefrompng($uploadImage);

  //--------------------------------------------------------
  //  ③imagecreatetruecolor関数でベースをつくり
  //    そこにサンプリングしてコピーする
  //--------------------------------------------------------
   $image_s=imagecreatetruecolor($width_s, $height_s);
   //サンプリングする
   $result=imagecopyresampled($image_s,$image,0,0,0,0,$width_s,$height_s,$width,$height);
  //--------------------------------------------------------
  //  ④imagejpg関数でブラウザあるいはファイルとして出力する
  //--------------------------------------------------------
  $status = 0;
  if($result){ //サンプリングによる画像作成成功
    if(imagepng($image_s,$thumb_name)){ //imagejpeg関数で画像を保存
      $status = 0;
    }else{
      $status -1;
    }
  }else{
    $status = -1;
  }
 //取り出した元画像とサンプリングのための作成したベースが画像を削除する
 imagedestroy($image);
 imagedestroy($image_s);
 return $status;
}
?>
