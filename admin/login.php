<?php
/**
* login.php
* View:ログイン画面
* Controller:index.php
*　
* @access public
*
**/
$g_Title = $gSys->getProperty("system", "title");
if (!isset($_errormessage)){
  $_errormessage = "";
}
?>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="NOINDEX,NOFOLLOW" />
  <meta http-equiv="x-ua-compatible" content="ie=edge">
<!-- favisonの設定
  <link rel="shortcut icon" href="../images/favicon.ico" type="image/vnd.microsoft.icon">
  <link rel="icon" href="../images/favicon.ico" type="image/vnd.microsoft.icon">
-->
<title><?= $g_Title ?> | Log in</title>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<style>
body, html {
  background: url(../images/loginback.jpg) no-repeat center center fixed;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}
.card-container.card {
    max-width: 450px;
    padding: 25px 25px;
}

.btn {
    font-weight: 700;
    height: 36px;
    -moz-user-select: none;
    -webkit-user-select: none;
    user-select: none;
    cursor: default;
}

/*
 * Card component
 */
.card {
    background-color: #F7F7F7;
    /* just in case there no content*/
    padding: 20px 15px 30px;
    margin: 0 auto 15px;
    margin-top: 50px;
    /* shadows and rounded borders */
    -moz-border-radius: 2px;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
}

.profile-img-card {
    width: 96px;
    height: 96px;
    margin: 0 auto 10px;
    display: block;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
}

/*
 * Form styles
 */
.profile-name-card {
    font-size: 16px;
    font-weight: bold;
    text-align: center;
    margin: 10px 0 0;
    min-height: 1em;
}

.reauth-loginid {
    display: block;
    color: #404040;
    line-height: 2;
    margin-bottom: 10px;
    font-size: 14px;
    text-align: center;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
}

.form-signin #inputEmail,
.form-signin #inputPassword {
    direction: ltr;
    height: 44px;
    font-size: 16px;
}

.form-signin input[type=login],
.form-signin input[type=password],
.form-signin input[type=text],
.form-signin button {
    width: 100%;
    display: block;
    margin-bottom: 10px;
    z-index: 1;
    position: relative;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
}

.form-signin .form-control:focus {
    border-color: rgb(104, 145, 162);
    outline: 0;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgb(104, 145, 162);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgb(104, 145, 162);
}

.btn.btn-signin {
    /*background-color: #4d90fe; */
    background-color: rgb(104, 145, 162);
    /* background-color: linear-gradient(rgb(104, 145, 162), rgb(12, 97, 33));*/
    padding: 0px;
    font-weight: 700;
    font-size: 14px;
    height: 36px;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    border: none;
    -o-transition: all 0.218s;
    -moz-transition: all 0.218s;
    -webkit-transition: all 0.218s;
    transition: all 0.218s;
}

.btn.btn-signin:hover,
.btn.btn-signin:active,
.btn.btn-signin:focus {
    background-color: rgb(12, 97, 33);
}

.forgot-password {
    color: rgb(104, 145, 162);
}

.forgot-password:hover,
.forgot-password:active,
.forgot-password:focus{
    color: rgb(12, 97, 33);
}
</style>
  </head>
  <body id="LoginForm">
  <div class="container">
      <div class="card card-container">
          <!-- img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" / -->
          <div align="center"><img src="../images/artlogo.png" width="125px"/></div>
          <p id="profile-name" class="profile-name-card"><?= $g_Title ?>システム</p>
          <div class="card-body">
          <div class="card-text text-danger text-center strong"><small><?= $_errormessage ?></small></div>
          <br>
          <div class="card-text text-center"><small>メールアドレスとパスワードを入力してください</small></div>
          <form class="form-signin" action="index.php" method="POST">
            <input type="hidden" name="mode" value="LOGIN">
            <span id="reauth-loginid" class="reauth-loginid"></span>
            <div class="form-group">
              <input type="email" class="form-control" name="login" placeholder="メールアドレスを入力してください" required>
            </div>
            <div class="form-group">
              <input type="password" name="password" class="form-control" id="inputPassword" placeholder="パスワードを入力してください">
            </div>
              <button type="submit" class="btn btn-lg btn-block btn-primary btn-signin">Login</button>
          </form><!-- /form -->
          <div class="card-text text-danger text-center strong"><small>パスワードは５回連続して誤入力するとロックされるので<br>ご注意ください。</small></div>
      </div><!-- /card-container -->
    </div>
  </div><!-- /container -->
</body>
</html>
