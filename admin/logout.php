<?php
/**
* login.php
* View:ログアウト処理
*　
* @access public
*
**/
require_once("../inc/admin.inc.php");
$_SESSION = array();

// Note: セッション情報だけでなくセッションを破壊する。
if (isset($_COOKIE[session_name()])) {
    setcookie(session_name(), '', time()-42000, '/');
}
// 最終的に、セッションを破壊する
session_destroy();
header("HTTP/1.1 301 Moved Permanently");
header("Location: index.php");
exit();
?>
