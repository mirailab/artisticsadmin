<?php
function h($var)  // HTMLでのエスケープ処理をする関数
{
  if (is_array($var)) {
    return array_map('h', $var);
  } else {
    return htmlspecialchars($var, ENT_QUOTES, 'UTF-8');
  }
}
//var_dump($_POST);
if (isset($_POST['mode']) && $_POST['mode'] == "ISSUE"){
  if (isset($_POST['rawpass']) && strlen($_POST['rawpass']) > 8){
    $options = array("cost"=>10);
    $raw_pass = h($_POST['rawpass']);
    $hash_pass = password_hash($raw_pass, PASSWORD_DEFAULT, $options);
    //print "hash_pass=>$hass_pass<br>";
  }
}
?>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="NOINDEX,NOFOLLOW" />
  <meta http-equiv="x-ua-compatible" content="ie=edge">
<title>管理画面パスワード発行</title>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<style>
.card-container.card {
    max-width: 450px;
    padding: 25px 25px;
}
.btn {
    font-weight: 700;
    height: 36px;
    -moz-user-select: none;
    -webkit-user-select: none;
    user-select: none;
    cursor: default;
}
/*
 * Card component
 */
.card {
    background-color: #F7F7F7;
    /* just in case there no content*/
    padding: 20px 15px 30px;
    margin: 0 auto 15px;
    margin-top: 50px;
    /* shadows and rounded borders */
    -moz-border-radius: 2px;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
}

.password-card {
    width: 96px;
    height: 96px;
    margin: 0 auto 10px;
    display: block;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
}

/*
 * Form styles
 */
.profile-card {
    font-size: 16px;
    font-weight: bold;
    text-align: center;
    margin: 10px 0 0;
    min-height: 1em;
}
.form-signin #inputEmail,
.form-signin #inputPassword {
    direction: ltr;
    height: 44px;
    font-size: 16px;
}

.form-signin input[type=login],
.form-signin input[type=password],
.form-signin input[type=text],
.form-signin button {
    width: 100%;
    display: block;
    margin-bottom: 10px;
    z-index: 1;
    position: relative;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
}

.form-signin .form-control:focus {
    border-color: rgb(104, 145, 162);
    outline: 0;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgb(104, 145, 162);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgb(104, 145, 162);
}

.btn.btn-signin {
    padding: 0px;
    font-weight: 700;
    font-size: 14px;
    height: 36px;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    border: none;
    -o-transition: all 0.218s;
    -moz-transition: all 0.218s;
    -webkit-transition: all 0.218s;
    transition: all 0.218s;
}
.btn.btn-signin:hover,
.btn.btn-signin:active,
.btn.btn-signin:focus {
    background-color: rgb(12, 97, 33);
}
</style>
  </head>
  <body>
  <div class="container">
      <div class="card card-container">
          <div class="card-body">
            <h6 class="text-center">パスワードハッシュ文字列発行</h6>
<?php
if (!isset($_POST['mode'])){
?>
          <div class="card-text text-center"><small>パスワード文字列を入力してください<span class="text-danger">※8文字以上</span></small></div>
          <form class="form-signin" action="issue_password.php" method="POST">
            <input type="hidden" name="mode" value="ISSUE">
            <p></p>
            <div class="form-group">
              <input type="text" class="form-control" name="rawpass" placeholder="パスワードに使用する文字列を入力してください" required>
            </div>
              <button type="submit" class="btn btn-lg btn-block btn-primary btn-signin">ハッシュ化する</button>
          </form><!-- /form -->
<?php
}else {
?>
          <p></p>
          <div class="card-text text-center"><small>以下の文字列をconfig.iniに貼り付けてください</small></div>
          <p></p>
          <form>
            <div class="form-group">
              <input type="text" class="form-control" value="<?= $hash_pass ?>" disabled>
            </div>
            <div class="form-group">
              <div class="card-text text-center text-danger"><small>元の文字列は忘れないようにメモしてください</small></div>
              <input type="text" class="form-control" value="<?= $raw_pass ?>" disabled>
            </div>
          </form><!-- /form -->
<?php
}
?>
</div><!-- /card-container -->
</div>
  </div><!-- /container -->
</body>
</html>
