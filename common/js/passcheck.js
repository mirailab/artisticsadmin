/**************************************************/
/* パスワードチェック用
/**************************************************/
// 記号が含まれているか
function validateSign(val) {
//    var reg = new RegExp(/[!"#$%&'()\*\+\-\.,\/:;<=>?@\[\\\]^_`{|}~]/g);
    var reg = new RegExp(/[!@#$%]/g);
    if(reg.test(val)) {
      return true;
    }
      return false;
}
// 英小文字が含まれているか
function validateAlpha(val) {
    var reg = new RegExp(/[a-z]/g);
    if(reg.test(val)) {
      return true;
    }
    return false;
}
// 英大文字が含まれているか
function validateCapAlpha(val) {
  var reg = new RegExp(/[A-Z]/g);
  if(reg.test(val)) {
    return true;
  }
    return false;
}
// 英数字が含まれているか
function validateNumber(val) {
  var reg = new RegExp(/[0-9]/g);
  if(reg.test(val)) {
    return true;
  }
    return false;
}
