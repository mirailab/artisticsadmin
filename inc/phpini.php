<?php
    ini_set("include_path", "./:../:./inc/:../inc:./common/:../common:./Classes:../Classes:../:./lib:../lib:../lib/PEAR:../logic:/usr/share/php:" . ini_get("include_path"));
    mb_language('ja');
    ini_set('memory_limit', '-1');  // -1 here implies no limit, you can even set it to a bigger number like '192M' for 192Mb space.
    ini_set('mbstring.internal_encoding', 'UTF8');
    ini_set('mbstring.detect_order', 'auto');
    ini_set('mbstring.http_input', 'auto');
    ini_set('mbstring.http_output', 'UTF8');
    ini_set('session.gc_maxlifetime', 86400 );  // 秒(デフォルト:1440)

    ini_set( 'display_errors', 1 );
    // エラー（スクリプトの実行が中断される）のみ出力する場合
    ini_set( 'error_reporting', E_ERROR);

    @session_start();

    require_once("Sys.class.php");
  //  print "base=>".$base."<br>";
    $gSys = new Sys("./conf/config.ini");
    $gSys->init();
?>
