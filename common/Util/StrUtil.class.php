<?php
/**
 * 文字関係ユーティリティ
 * --------------------------------------------------------
 */
class StrUtil {
  /**
   * Email形式チェック
   * @access public
   * @param string $email
   * @return true:チェックOK false:チェックNG
   */
  static function checkEmail($email) {
    $ok =
        ereg("^[0-9,A-Z,a-z][0-9,a-z,A-Z,_,\.,-]+@"
        . "[0-9,A-Z,a-z][0-9,a-z,A-Z,_,\.,-]+\."
        . "(af|al|dz|as|ad|ao|ai|aq|ag|ar|am|aw|ac|au|at|az|"
        . "bh|bd|bb|by|bj|bm|bt|bo|ba|bw|br|io|bn|bg|bf|bi|kh|"
        . "cm|ca|cv|cf|td|gg|je|cl|cn|cx|cc|co|km|cg|cd|ck|cr|"
        . "ci|hr|cu|cy|cz|dk|dj|dm|do|tp|ec|eg|sv|gq|er|ee|et|"
        . "fk|fo|fj|fi|fr|gf|pf|tf|fx|ga|gm|ge|de|gh|gi|gd|gp|gu|"
        . "gt|gn|gw|gy|ht|hm|hn|hk|hu|is|in|id|ir|iq|ie|im|il|it|"
        . "jm|jo|kz|ke|ki|kp|kr|kw|kg|la|lv|lb|ls|lr|ly|li|lt|lu|"
        . "mo|mk|mg|mw|my|mv|ml|mt|mh|mq|mr|mu|yt|mx|fm|md|mc|mn|"
        . "ms|ma|mz|mm|na|nr|np|nl|an|nc|nz|ni|ne|ng|nu|nf|mp|no|"
        . "om|pk|pw|pa|pg|py|pe|ph|pn|pl|pt|pr|qa|re|ro|ru|rw|kn|"
        . "lc|vc|ws|sm|st|sa|sn|sc|sl|sg|sk|si|sb|so|za|gs|es|lk|"
        . "sh|pm|sd|sr|sj|sz|se|ch|sy|tw|tj|tz|th|bs|ky|tg|tk|to|"
        . "tt|tn|tr|tm|tc|tv|ug|ua|ae|uk|us|um|uy|uz|vu|va|ve|vn|"
        . "vg|vi|wf|eh|ye|yu|zm|zw|"
        . "com|net|org|gov|edu|int|mil|biz|info|name|pro|jp)"
        . "$",$email);
    return $ok;
  }
  static function checkMailPattern($email){
    $pattern = '/\A([a-z0-9_\-\+\/\?]+)(\.[a-z0-9_\-\+\/\?]+)*' .
               '@([a-z0-9\-]+\.)+[a-z]{2,6}\z/i';
    if (! preg_match($pattern, $email)) {
      return 0;
    }
    return 1;
  }

  /**
   * "stringUtil"�Τ褦��ʸ�����"string_util"���Ѵ�����
   * @access public static
   * @param string $str
   * @return string
   */
  static function uc2us($str) {
    /* ʸ����������ʬ�� */
    $chars = StrUtil::str2array($str);
    $ret = array();
    $first = true;
    foreach ($chars as $char) {
      if (($char >= "A") && ($char <= "Z")) {
        /* ��ʸ������ե��٥åȤ��ä��� */
        if ($first == false) {
          /* ��Ƭ����ʤ����_���ɲ� */
          $ret[] = "_";
        }
        /* ��ʸ���ˤ��� */
        $ret[] = strtolower($char);
      }
      else {
        $ret[] = $char;
      }
      $first = false;
    }

    return join("", $ret);
  }

  /**
   * "string_util"�Τ褦��ʸ�����"stringUtil"���Ѵ�����"
   * @access public
   * @param string $str
   * @return string
   */
  static function us2uc($str) {
    /* ʸ����������ʬ�� */
    $chars = StrUtil::str2array($str);
    $ret = array();
    $uc = false;
    foreach ($chars as $char) {
      if ($char == "_") {
        $uc = true;
      }
      else if ($uc) {
        $ret[] = strtoupper($char);
        $uc = false;
      }
      else {
        $ret[] = $char;
      }
    }
    var_dump($ret);
    return join("", $ret);
  }

  /**
   * 文字列を１文字ずつ配列に格納
   * @access public
   * @param string $str
   * @return array
   */
  static function str2array($str) {
    $length = strlen($str);
    $chars = array();
    for ($i = 0; $i < $length; $i++) {
      $chars[$i] = $str{$i};
    }
    return $chars;
  }

	/**
	 * 「半角」カナを「全角」に変換
	 * @access public static
	 * @param string $str
	 * @return string
	 */
	static function convertHanZenKana($str) {
		return mb_convert_kana($str);
	}

	/**
	 * 「全角」英数字を「半角」変換
	 * @access public static
	 * @param string $str
	 * @return string
	 */
	static function convertZenHanEiNum($str) {
		return mb_convert_kana($str, "a");
	}

  /**
   * 「全角」カナ英数字を「半角」変換
   * @access public
   * @param string
   * @return string
   */
  static function correctZenHan($str) {
    return StrUtil::convertZenHanEiNum(StrUtil::convertHanZenKana($str));
  }

  /**
   *
   * @access public static
   * @param string $str
   * @return true:�Ҥ餬�ʤ��� false:�Ҥ餬�ʤ�������ʤ�
   */
  static function isOnlyHiragana($str) {
    return mb_ereg("[^��-��]", $str) === false;
  }

  /**
   *
   * @access public static
   * @param string $str
   * @return true:�������ʤ��� false:�������ʤ�������ʤ�
   */
  static function isOnlyKatakana($str) {
    return mb_ereg("[^��-����0-9�� ]", $str) === false;
  }

  /**
   * 英数字以外が含まれていないかチェック
   * @access public static
   * @param string $str
   * @return true:英数字以外が含まれていない
   */
  static function isOnlyEisu($str) {
    return mb_ereg("[^a-zA-Z0-9]", $str) === false;
  }

  /**
   * URLかチェック
   * @access public static
   * @param string $str
   * @return true:OK false:NG
   */
  static function isUrl($str) {
    return mb_ereg("[^-_$.+!*'(),a-zA-Z0-9]", $str) === false;
  }

  /**
   * 英数字ハイフン以外が含まれていないかチェック
   * @access public static
   * @param string $str
   * @return true:OK false:NG
   */
  static function isNumHyphen($str) {
    return mb_ereg("[^-0-9]", $str) === false;
  }

  /**
   * 英数字ハイフン、アンダースコア以外が含まれていないかチェック
   * @access public
   * @param string $str
   * @return true:OK false:NG
   */
  function isEisuHyphUscore($str) {
    return mb_ereg("[^-_a-zA-Z0-9]", $str) === false;
  }

  /**
   * 文字列を所定の長さでカットし、$tailに指定した文字列（指定しなければ"..."）を末尾に付加する
   * @access public
   * @param string $str
   * @param int $len
   * @param string $tail
   */
  function shrink($str, $len, $tail = "...") {
    if (mb_strlen($str) > $len) {
      return mb_substr($str, 0, $len - 1) . $tail;
    }
    else {
      return $str;
    }
  }
  /**
   * 記号を半角から全角、または全角から半角に変換
   * @access public
   * @param string $str
   * @param int $option 0:半角から全角, 1:全角から半角
   */
  function convert_kigo_han_zen($str, $option) {
    // option=0:半角から全角, 1:全角から半角
    $pairs = array(
        array(" ", "　"), // 半角スペース⇔全角スペース
        array("!", "！"),
        array("#", "＃"),
        array("$", "＄"),
        array("%", "％"),
        array("&", "＆"),
        array("'", "’"),
        array("(", "（"),
        array(")", "）"),
        array("*", "＊"),
        array("+", "＋"),
        array(",", "，"),
        array("-", "－"),
        array(".", "．"),
        array("/", "／"),
        array(":", "："),
        array(";", "；"),
        array("<", "＜"),
        array("=", "＝"),
        array(">", "＞"),
        array("?", "？"),
        array("@", "＠"),
        array("[", "［"),
        array("]", "］"),
        array("^", "＾"),
        array("_", "＿"),
        array("`", "｀"),
        array("{", "｛"),
        array("|", "｜"),
        array("}", "｝"),
        array("~", "～"),
        array("｡", "。"),
        array("｢", "「"),
        array("｣", "」"),
        array("､", "、"),
        array("･", "・")
    );
    foreach($pairs as $pair) {
        $before = $pair[$option];
        $after = $pair[1 - $option];
        $str = str_replace($before, $after, $str);
    }
    return $str;
  }
}
?>
