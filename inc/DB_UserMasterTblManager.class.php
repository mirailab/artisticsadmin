<?php
/**
* DB_UserMasterTblManager
*　usermasterテーブル操作
* @package DB
* @author N.Tsuchimoto
*/
require_once("DBManager.class.php");

class DB_UserMasterTblManager extends DBManager{
    var $error;
    //コンストラクタ
    function __construct() {
        parent::__construct();
    }
  /**
  * doSelectAll
  *　全レコード検索　id(降順)
  * @return array テーブル行連想配列
  */
	public function doSelectAll(){
        $sql = "SELECT * FROM usermaster ORDER BY id DESC;";
        $stmh = $this->query($sql);
        $memberArray = array();
        if ($stmh->rowCount() > 0){
          while ($row = $stmh->fetch(PDO::FETCH_ASSOC)) {
              $memberArray[] = $row;
          }
        }  
        return $memberArray;
   	}
    /**
    * getUserByEmail
    *　メールアドレスでユーザを検索
    *　
    * @param string $email メールアドレス
    * @return resource $obj usermasterレコード
    */
    public function getUserByEmail($email){
//      $sql = "SELECT * FROM usermaster WHERE email=:email ;";
      $sql = sprintf("SELECT * FROM usermaster WHERE email=%s;", $this->sql_string($email));
      //print "sql=>$sql<br>";
      $stmh = $this->query($sql);
//      $stmh = $this->prepare($sql);
//      $stmh->bindParam(":email", $email, PDO::PARAM_STR);
//      $stmh->execute();
      if ($stmh->rowCount() > 0){
        $obj = $stmh->fetchObject();
        return $obj;
      }
      return NULL;
    }
    /**
    * insertUser
    *　インサート処理
    *　
    * @param array $insert_data インサートデータ(id以外のusermasterテーブルアイテムをキーにした連想配列)
    * @return resource $obj インサーソしたusermasterレコード
    */
    public function insertUser($insert_data){
      //print "insertdata=><br>";
      //var_dump($insert_data);
      $values = $keys = array();
      foreach($insert_data as $k => $v){
          $keys[] = $k;
          $values[] = $this->sql_string($v);
      }
      $keylist = implode(",", $keys);
      $valuelist = implode(",", $values);
  //print "keylist=>$keylist<br>";
  //print "valuelist=>$valuelist<br>";
      try{
          $this->begin();

          $sql = sprintf("INSERT INTO usermaster (%s) VALUES (%s);", $keylist, $valuelist);
  //print "sql=>$sql<br>";
          $stmh = $this->query($sql);

          $this->commit();

          list($id) = $this->query("select LAST_INSERT_ID();", PDO::FETCH_NUM)->fetch();
          //$sql = sprintf("SELECT * FROM rss_data WHERE owner_id = %d AND registtime ='%s';", $owner_id, $timestamp);
          $sql = sprintf("SELECT * FROM usermaster WHERE id = %d;", $id);
          $result = $this->query($sql);
          if ($result->rowCount() > 0){
            $obj = $result->fetchObject();
            return $obj;
          }
          return NULL;
      }catch (PDOException $e) {
          // エラー（例外）が発生した時の処理
          $errormsg  = sprintf("Insert Error[%s]",$e->getMessage());
          $this->error = $errormsg;
          //print "error=>".$this->error;

          return NULL;
      }
    }
    /**
    * checkDupEmail($email)
    *　emailの重複チェック
    *　
    * @param string $email　メールアドレス
    * @return resource $obj 所定メールアドレスのusermasterレコード
    */
    public function checkDupEmail($email){
        $sql = "SELECT * FROM usermaster WHERE email=:email ;";
        //print "sql=>$sql<br>";
        $stmh = $this->prepare($sql);
        $stmh->bindParam(":email", $email, PDO::PARAM_STR);
        $stmh->execute();
        if ($stmh->rowCount() <= 0){
            return NULL;
        }
        $obj = $stmh->fetchObject();
        return $obj;
    }
    /**
    * getUserById($id)
    *　idで検索
    *　
    * @param integer $id　usermaster.id
    * @return resource $obj 所定idのusermasterレコード
    */
    public function getUserById($id){
        $sql = "SELECT * FROM usermaster WHERE id=:id ;";
        $stmh = $this->prepare($sql);
        $stmh->bindValue(":id", $id, PDO::PARAM_INT);
        $stmh->execute();
        if ($stmh->rowCount() <= 0){
            return NULL;
        }
        $obj = $stmh->fetchObject();
        return $obj;
	}
  /**
  * updateUserById($id, $update_data)
  *　更新処理
  *　
  * @param integer $id　usermaster.id
  * @param integer $update_data　更新データ(id以外のusermasterテーブルアイテムをキーにした連想配列)
  * @return resource $obj 所定idのusermasterレコード
  */
  public function updateUserById($id, $update_data){
    //  print "updateMemberById=>$id<br>";
    //  print "update_data=><br>";
//var_dump($update_data);
        $sql = "";
        foreach($update_data as $key => $value){
            if ($sql != "" ) $sql .=", ";

            $sql .= $key." = ".$this->sql_string($value)." ";
        }
        try{
            $this->begin();

            $sql = "UPDATE usermaster SET ".$sql;
            $sql .= "WHERE id = ".$id.";";
            //print "<br>sql=>$sql<br>";

            $this->query($sql);
            $this->commit();
            $obj = self::getUserById($id);
            return $obj;
        }catch (PDOException $e) {
            // エラー（例外）が発生した時の処理
            $error = sprintf("Update Error[%s]" . $e->getMessage());
            //print $error;
            $this->error = $error;
            return NULL;
        }
    }
    /**
    * deleteUserById($id)
    *　削除処理
    *　
    * @param integer $id　usermaster.id
    * @return mixed NULL:正常終了　$error:エラーメッセージ
    */
    public function deleteUserById($id){
        try{
            $this->begin();
            $sql = "DELETE FROM usermaster WHERE id=:id ;";
            $stmh = $this->prepare($sql);
            $stmh->bindValue(":id", $id, PDO::PARAM_INT);
            $stmh->execute();
            $this->commit();
            return NULL;
        }catch (PDOException $e) {
            // エラー（例外）が発生した時の処理
            $error = sprintf("Delete Error[%s]" . $e->getMessage());
//            print $error;
            return $error;
        }
   	}
}
?>
