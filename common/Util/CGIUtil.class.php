<?php
class CGIUtil {
	# 入力値に不正なデータがないかチェック
	// Formのパラメータを受け取りハッシュ配列に格納
	static function getFormParm(){
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method == "POST"){
			$array = CGIUtil::cnvFormvalue($_POST);
		}else {
			$array = CGIUtil::cnvFormvalue($_GET);
		}
		return $array;
	}

	// 配列データを一括変換
	static function cnvFormvalue($array) {
    	foreach($array as $k => $v){
    		// 「magic_quotes_gpc = On」のときはエスケープ解除
        	if (get_magic_quotes_gpc()) {
            	$v = @stripslashes(trim($v));
        	}
        	$v = @htmlspecialchars(trim($v));
        	$array[$k] = $v;
    	}
    	return $array;
	}
	// 指定パラメータのデータを配列に格納
	static function getParamData($form, $param){
		$array = array();
		$pattern = sprintf('/^%s*/', $param);
		foreach($form as $key => $value){
			//print "key=>$key<br>";
			if (preg_match($pattern, $key)){
				$idx = str_replace($param, "", $key);
				$array[$idx]=trim($value);
			}
		}
		//var_dump($array);
		return $array;
	}
}
?>
