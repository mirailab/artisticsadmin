ArtisticsAdmin
======================
Bootstrapを使用した管理画面テンプレート

設定ファイル admin/cont/contig.ini
------
DBへの接続情報やタイトルなどはこのファイルに設定してください。

設定内容は、Sysクラスで取得することができます。
inc/phpini.phpで設定を読み込むための初期化を行っているのでこのファイルをインクルードしてください。

```
require_once("Sys.class.php");
$gSys = new Sys("./conf/config.ini");
$gSys->init();

$g_AdminTop = $gSys->getProperty("system", "adminurl");
```

管理サイトへのログイン情報を設定ファイルで定義する場合は、以下の様に記述します。
passwordはハッシュ文字列で記載するため、以下の画面でハッシュ文字列を取得してペーストしてください。

https://art3.sakura.ne.jp/labo/ArtisticsAdmin/issue_password.php

```
[admin]
; 管理サイトへのログイン情報
login = tsuchimoto-n@artistics.co.jp
password = $2y$10$wgMbsqgbMlyhh2zGOJIt5OpRPnTazbogTEclWVYtrrlLh3JGC.SJ.
;
```

../inc/admin.inc.php
----------------
ログイン後はセッション変数でログイン状態を保持して、ログインしていないか、または一定時間経過後はログアウトします。
これらの処理はadmin.inc.phpで行っているため、このファイルを先頭でインクルードしてください。  
その場合はphpini.phpファイルをインクルードする必要はありません。


ログイン画面の背景画像、ロゴ
----------------
imagesフォルダのloginback.jpgとartlogo.pngを差し替えてください。


DB操作クラス
../inc/DBManager.class.php
----------------
テーブルごとにDBManager.class.phpを継承したクラスを作成することができます。

ページの共通ヘッダ＆フッター
common_header.php
common_footer.php
----------------
必要なスタイルシートやJSを定義しています。
サイドメニューとトップバーはcommon_header.phpをインクルードすれば表示されます。

トップバーの色を変える場合は、
```
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
```
bg-whiteを例えばbg-primaryに変更するとトップバーの色が青色になります。
